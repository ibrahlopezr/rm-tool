using helpers;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Forms;
using Claunia.PropertyList;
using Renci.SshNet;

public class iCloudByPass
{
	[CompilerGenerated]
	private sealed class Class10
	{
		public MainForm mainForm_0;

		public int int_0;

		internal void method_0()
		{
			mainForm_0.SetPercentaje(int_0, int_0.ToString());
		}
	}

	private SshClient sshClient;

	private ScpClient scpClient;

	public string Host = "127.0.0.1";

	public string SSH_Password = "alpine";

	public int Port = 22;

	private string strOne = "";

	private string strTwo = "";

	private string strThree = "";

	private string strFour = "";

	private string strFive;

	private string var_mobile_library_preferences = "/var/mobile/Library/Preferences/";

	private string var_wireless_library_preferences = "/var/wireless/Library/Preferences/";

	private string var_root_library_lockdownd = "/var/root/Library/Lockdown/";

	private string activation_record_plist = "///activation_record.plist";

	private string device_specific_nobackup_plist = "///com.apple.commcenter.device_specific_nobackup.plist";

	private string data_ark_plist = "///data_ark.plist";

	private string data_ark_plist2 = "///data_ark.plist";

	private string com_apple_purplebuddy_plist = "///com.apple.purplebuddy.plist";

	public static string CurrentDirectory = Directory.GetCurrentDirectory();

	public static string vendor_other = CurrentDirectory + "/vendor/other/";

	public static string tmp_backup = "/tmp/Backup";

	private bool bool_0 = true;


	public void BypassGSM()
	{
		try
		{
			ClearOtherDirectory();
			VerifyPairing();
			ShowPercentage(1);
			SshConnect();
			ShowPercentage(2);
			method_20();
			ShowPercentage(3);
			method_12();
			ShowPercentage(4);

			RemoveFolders();

			ShowPercentage(5);

			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/untethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/untethered.plist");

			ShowPercentage(6);

			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.plist");

			ShowPercentage(7);

			DeviceDeactivate();

			ShowPercentage(8);

			ChangeFlagsFiles();

			ShowPercentage(9);

			LaunchDaemons();

			ShowPercentage(14);

			method_25();
			ShowPercentage(15);
			UploadResourceFile(Resource.GSM_RAPTOR, "/System/Library/PrivateFrameworks/MobileActivation.framework/Support/Certificates/RaptorActivation.pem");

			ShowPercentage(16);
			CreateDirectories();
			ShowPercentage(17);
			SshRunCommand("launchctl load /System/Library/LaunchDaemons/*");
			ShowPercentage(18);
			Thread.Sleep(1000);
			ShowPercentage(20);
			if (!DeviceIsActivated())
			{
				MessageBox.Show("Error número 1, lo que significa que la herramienta no puede copiar sus archivos. Realice un software o restaure su dispositivo y vuelva a intentarlo ", "Error in Activation Process (1)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			ShowPercentage(27);
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/simple.dylib && rm /Library/MobileSubstrate/DynamicLibraries/simple.plist");
			ShowPercentage(28);
			method_16();
			ShowPercentage(30);
			UploadResourceFile(Resource.UNTETHERED, "/Library/MobileSubstrate/DynamicLibraries/untethered.dylib");
			ShowPercentage(33);
			UploadResourceFile(Resource.UNTETHERED1, "/Library/MobileSubstrate/DynamicLibraries/untethered.plist");
			ShowPercentage(35);
			SshRunCommand("chmod +x /Library/MobileSubstrate/DynamicLibraries/untethered.dylib");
			ShowPercentage(40);
			LoadFilesForActivation();
			ShowPercentage(45);
			SshRunCommand("killall -9 SpringBoard mobileactivationd");
			ShowPercentage(50);
			Thread.Sleep(2000);
			ShowPercentage(53);
			UploadResourceFile(Resource.IUNTETHERED, "/Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib");
			ShowPercentage(54);
			UploadResourceFile(Resource.IUNTETHERED1, "/Library/MobileSubstrate/DynamicLibraries/iuntethered.plist");
			ShowPercentage(57);
			SshRunCommand("chmod +x /Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib");
			ShowPercentage(60);
			SshRunCommand("killall backboardd");
			ShowPercentage(63);
			Thread.Sleep(2000);
			ShowPercentage(67);
			SshRunCommand("chflags nouchg " + com_apple_purplebuddy_plist);
			ShowPercentage(68);
			try
			{
				method_12();
				ShowPercentage(71);
				download_device_specific_nobackup_file();
				ShowPercentage(75);
				WritingActivationRecords();
				ShowPercentage(79);
				AddkPostponementTicket();
				ShowPercentage(82);
				method_22();
				ShowPercentage(85);
				method_23();
				ShowPercentage(88);
			}
			catch (Exception ex)
			{
				ClearOtherDirectory();
				MessageBox.Show("Errror 2 restaura tu dispositivo con 3utools", "Error in Activation Process (2)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				MessageBox.Show(ex.Message, "Error in Activation Process (2)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/untethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/untethered.plist");
			ShowPercentage(90);
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.plist");
			ShowPercentage(94);
			RemoveFolders();
			ShowPercentage(96);
			method_5();
			ShowPercentage(98);
			ClearOtherDirectory();
			ShowPercentage(100);
			new SuccessBox2().ShowDialog();
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.Message.ToString());
			MessageBox.Show("Dispositivo iOS Con Jailbreak Falloso realize jailbreak y vuelva a intentar", "Error in Activation Process (1)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
	}

	public void BypassMEID()
	{
		try
		{
			ClearOtherDirectory();
			VerifyPairing();
			ShowPercentage(1);
			SshConnect();
			ShowPercentage(2);
			method_20();
			ShowPercentage(3);
			method_12();
			ShowPercentage(4);
			RemoveFolders();
			ShowPercentage(5);
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/untethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/untethered.plist");
			ShowPercentage(6);
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.plist");
			ShowPercentage(7);
			DeviceDeactivate();
			ShowPercentage(8);
			ChangeFlagsFiles();
			ShowPercentage(9);
			LaunchDaemons();
			ShowPercentage(14);
			method_25();
			ShowPercentage(15);
			UploadResourceFile(Resource.GSM_RAPTOR, "/System/Library/PrivateFrameworks/MobileActivation.framework/Support/Certificates/RaptorActivation.pem");
			ShowPercentage(16);
			CreateDirectories();
			ShowPercentage(17);
			SshRunCommand("launchctl load /System/Library/LaunchDaemons/*");
			ShowPercentage(18);
			Thread.Sleep(1000);
			ShowPercentage(20);
			if (!DeviceIsActivated())
			{
				MessageBox.Show("Error número 1, lo que significa que la herramienta no puede copiar sus archivos. Haga un software o restaure su dispositivo y vuelva a intentarlo", "Error in Activation Process (1)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				ShowPercentage(0);
				//return;
			}
			ShowPercentage(27);
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/simple.dylib && rm /Library/MobileSubstrate/DynamicLibraries/simple.plist");
			ShowPercentage(28);
			method_16();
			ShowPercentage(30);
			UploadResourceFile(Resource.UNTETHERED, "/Library/MobileSubstrate/DynamicLibraries/untethered.dylib");
			ShowPercentage(33);
			UploadResourceFile(Resource.UNTETHERED1, "/Library/MobileSubstrate/DynamicLibraries/untethered.plist");
			ShowPercentage(35);
			SshRunCommand("chmod +x /Library/MobileSubstrate/DynamicLibraries/untethered.dylib");
			ShowPercentage(40);
			LoadFilesForActivation();
			ShowPercentage(45);
			SshRunCommand("killall -9 SpringBoard mobileactivationd");
			ShowPercentage(50);
			Thread.Sleep(2000);
			ShowPercentage(53);
			UploadResourceFile(Resource.IUNTETHERED, "/Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib");
			ShowPercentage(54);
			UploadResourceFile(Resource.IUNTETHERED1, "/Library/MobileSubstrate/DynamicLibraries/iuntethered.plist");
			ShowPercentage(57);
			SshRunCommand("chmod +x /Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib");
			ShowPercentage(60);
			SshRunCommand("killall backboardd");
			ShowPercentage(63);
			Thread.Sleep(2000);
			ShowPercentage(67);
			SshRunCommand("chflags nouchg " + com_apple_purplebuddy_plist);
			ShowPercentage(68);
			try
			{
				method_12();
				ShowPercentage(71);
				download_device_specific_nobackup_file();
				ShowPercentage(75);
				WritingActivationRecords();
				ShowPercentage(79);
				AddkPostponementTicket();
				ShowPercentage(82);
				method_22();
				ShowPercentage(85);
				method_23();
				ShowPercentage(88);
			}
			catch (Exception ex)
			{
				ClearOtherDirectory();
				MessageBox.Show("Error número 2, que significa que la herramienta no puede transferir sus archivos. La solución es que la está ejecutando desde una interfaz de partición diferente ", "Error in Activation Process (2)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				MessageBox.Show(ex.Message, "Error in Activation Process (2)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
				return;
			}
			method_2();
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/untethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/untethered.plist");
			ShowPercentage(90);
			SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.dylib && rm /Library/MobileSubstrate/DynamicLibraries/iuntethered.plist");
			ShowPercentage(94);
			RemoveFolders();
			ShowPercentage(96);
			method_5();
			ShowPercentage(98);
			ClearOtherDirectory();
			ShowPercentage(100);
			new SuccessBox2().ShowDialog();
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.Message.ToString());
			MessageBox.Show("Jailbreak Falloso haga jailbreak de nuevo y vuelva a intentarlo ", "Error in Activation Process (1)", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
		}
	}

	public void method_2()
	{
		SshRunCommand("mv /usr/local/standalone/firmware/Baseband /usr/local/standalone/firmware/Baseband2");
		SshCommand sshCommand = SshRunCommand("find /private/preboot -type d -name \"Baseband\"");
		strOne = sshCommand.Result.Replace("/standalone/firmware/Baseband", "").Replace("\n", "")
			.Replace("//", "/");
		SshRunCommand("mount -o rw,union,update /private/preboot");
		SshRunCommand("mv " + strOne + "/standalone/firmware/Baseband " + strOne + "/standalone/firmware/Baseband2");
	}

	public void CreateDirectories()
	{
		SshRunCommand("mkdir -p " + var_wireless_library_preferences);
		ShowPercentage(18);
		SshRunCommand("mkdir -p " + strFour);
		ShowPercentage(19);
		SshRunCommand("mkdir -p " + strThree);
		ShowPercentage(20);
		SshRunCommand("mkdir -p " + var_root_library_lockdownd);
		ShowPercentage(21);
	}

	public void download_device_specific_nobackup_file()
	{
		downloadFiles(device_specific_nobackup_plist, vendor_other + "com.apple.commcenter.device_specific_nobackup.plist");
	}

	public void method_5()
	{
		method_20();
		if (iPhoneInfo.ProductVersion.StartsWith("13.") || iPhoneInfo.ProductVersion.StartsWith("14."))
		{
			SshRunCommand("mkdir -p /System/Library/PrivateFrameworks/Settings/GeneralSettingsUI.framework");
			SshRunCommand("chmod -R 777 /System/Library/PrivateFrameworks/Settings/GeneralSettingsUI.framework");
			UploadResourceFile(Resource.IOS13_RESET_PLIST, "/System/Library/PrivateFrameworks/Settings/GeneralSettingsUI.framework/Reset.plist");
		}
		else
		{
			SshRunCommand("mkdir -p /System/Library/PrivateFrameworks/PreferencesUI.framework");
			SshRunCommand("chmod -R 777 /System/Library/PrivateFrameworks/PreferencesUI.framework");
			UploadResourceFile(Resource.IOS12_RESET_PLIST, "System/Library/PrivateFrameworks/PreferencesUI.framework/Reset.plist");
		}
	}

	public void ShowPercentage(int int_1, string string_18 = "")
	{
		MainForm main = (MainForm)Application.OpenForms["MainForm"];
		main.Invoke((MethodInvoker)delegate
		{
			main.SetPercentaje(int_1, int_1.ToString());
		});
		if (string_18 == "")
		{
			int_1.ToString();
		}
		else
		{
			string_18 = int_1 + " >>> " + string_18;
		}
		Console.WriteLine(string_18);
	}

	public void downloadFiles(string OriginPath = "", string DestinationPath = "")
	{
		Stream stream = File.Create(DestinationPath);
		try
		{
			scpClient.Download(OriginPath, stream);
		}
		catch (Exception value)
		{
			Console.WriteLine(value);
			throw;
		}
		stream.Close();
	}

	public void VerifyPairing()
	{
		while (!method_9("pair"))
		{
			MessageBox.Show("Dele al boton confiar", "Dele al boton confiar encienda la pantalla de su dispositivo", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
		}
	}

	public bool method_9(string string_18)
	{
		string currentDirectory = Directory.GetCurrentDirectory();
		try
		{
			Process process;
			if (string_18 == "pair")
			{
				(process = new Process()).StartInfo = new ProcessStartInfo
				{
					FileName = currentDirectory + "\\vendor\\idevicepair.exe",
					Arguments = "pair",
					UseShellExecute = false,
					RedirectStandardOutput = true,
					CreateNoWindow = true
				};
			}
			else
			{
				(process = new Process()).StartInfo = new ProcessStartInfo
				{
					FileName = currentDirectory + "\\vendor\\idevicepair.exe",
					Arguments = "validate",
					UseShellExecute = false,
					RedirectStandardOutput = true,
					CreateNoWindow = true
				};
			}
			Process process2 = process;
			try
			{
				process2.Start();
				StreamReader standardOutput = process2.StandardOutput;
				string text = standardOutput.ReadToEnd();
				Thread.Sleep(2000);
				try
				{
					process2.Kill();
				}
				catch
				{
				}
				if (text.Contains("SUCCESS"))
				{
					standardOutput.Dispose();
					return true;
				}
				return false;
			}
			catch
			{
			}
		}
		catch (Win32Exception)
		{
			MessageBox.Show("Desconecte su dispositivo y revisa tu conexion a internet");
			return false;
		}
		return false;
	}

	public void method_10()
	{
		verify_known_hosts_file();
		try
		{
			Process process = new Process();
			process.StartInfo = new ProcessStartInfo
			{
				FileName = CurrentDirectory + "\\vendor\\iproxy.exe",
				Arguments = Port + " 44",
				UseShellExecute = false,
				RedirectStandardOutput = true,
				CreateNoWindow = true
			};
			process.Start();
		}
		catch (Win32Exception)
		{
			MessageBox.Show("مش لاقى ملفات البرنامج انت نقلتها فين يا بوب");
		}
	}

	public void verify_known_hosts_file()
	{
		Process[] processesByName = Process.GetProcessesByName("iproxy");
		for (int i = 0; i < processesByName.Length; i++)
		{
			processesByName[i].Kill();
		}
		if (File.Exists("%USERPROFILE%\\.ssh\\known_hosts"))
		{
			File.Delete("%USERPROFILE%\\.ssh\\known_hosts");
		}
	}

	public void method_12()
	{
		SshCommand sshCommand = SshRunCommand("find /private/var/containers/Data/System/ -iname 'internal'");
		strTwo = sshCommand.Result.Replace("Library/internal", "").Replace("\n", "")
			.Replace("//", "/");
		strThree = strTwo + "Library/internal/";
		strFour = strTwo + "Library/activation_records/";
		var_mobile_library_preferences = "/var/mobile/Library/Preferences/";
		var_wireless_library_preferences = "/var/wireless/Library/Preferences/";
		var_root_library_lockdownd = "/var/root/Library/Lockdown/";
		data_ark_plist2 = strThree + "data_ark.plist";
		activation_record_plist = strFour + "activation_record.plist";
		com_apple_purplebuddy_plist = var_mobile_library_preferences + "com.apple.purplebuddy.plist";
		device_specific_nobackup_plist = var_wireless_library_preferences + "com.apple.commcenter.device_specific_nobackup.plist";
		data_ark_plist = var_root_library_lockdownd + "data_ark.plist";
	}

	public bool DeviceIsActivated()
	{
        string activator_plist_ws = $"{helpers.helpers.getUri}/activator.php";
		using (Process process = new Process())
		{
			process.StartInfo.FileName = CurrentDirectory + "\\vendor\\ideviceactivation.exe";
			process.StartInfo.Arguments = "activate -s " + activator_plist_ws;
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			process.StartInfo.CreateNoWindow = true;
			process.Start();
			process.StandardOutput.ReadToEnd();
			process.WaitForExit();
		}
		string text = "";
		using (Process process2 = new Process())
		{
			process2.StartInfo.FileName = CurrentDirectory + "\\vendor\\ideviceactivation.exe";
			process2.StartInfo.Arguments = "state";
			process2.StartInfo.UseShellExecute = false;
			process2.StartInfo.RedirectStandardOutput = true;
			process2.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			process2.StartInfo.CreateNoWindow = true;
			process2.Start();
			text = process2.StandardOutput.ReadToEnd();
			process2.WaitForExit();
		}
		return text.Contains("Activated");
	}

	public void LoadFilesForActivation()
	{
		SshRunCommand("launchctl unload /System/Library/LaunchDaemons/com.apple.mobileactivationd.plist");
		SshRunCommand("launchctl load /System/Library/LaunchDaemons/com.apple.mobileactivationd.plist");
	}

	public void ChangeFlagsFiles()
	{
		SshRunCommand("chflags nouchg " + activation_record_plist + " && rm " + activation_record_plist);
		SshRunCommand("chflags nouchg " + device_specific_nobackup_plist + " && rm " + device_specific_nobackup_plist);
		SshRunCommand("chflags nouchg " + data_ark_plist + " && rm " + data_ark_plist);
		SshRunCommand("chflags nouchg " + data_ark_plist2 + " && rm " + data_ark_plist2);
		SshRunCommand("chflags nouchg " + com_apple_purplebuddy_plist + " && rm " + com_apple_purplebuddy_plist);
	}

	public void method_16()
	{
		SshRunCommand("rm /sbin/lzma");
		SshRunCommand("rm /foo.tar.lzma");
		SshRunCommand("rm /foo.tar");
		UploadResourceFile(Resource.LZMA, "/sbin/lzma");
		SshRunCommand("chmod 777 /sbin/lzma");
		UploadResourceFile(Resource.SUBSTRATE_GSM_1, "/foo.tar.lzma");
		SshRunCommand("lzma -d -v /foo.tar.lzma");
		SshRunCommand("tar -xvf /foo.tar -C /");
		SshRunCommand("rm /foo.tar.lzma");
		SshRunCommand("rm /foo.tar");
		SshRunCommand("/usr/libexec/substrate");
	}

	public bool UploadResourceFile(byte[] byte_0, string string_18)
	{
		try
		{
			if (!((BaseClient)scpClient).IsConnected)
			{
				((BaseClient)scpClient).Connect();
			}
			MemoryStream source = new MemoryStream(byte_0);
			scpClient.Upload((Stream)source, string_18);
			return true;
		}
		catch (Exception)
		{
			throw new ApplicationException("Error Upload Resource File");
		}
	}

	public void SshConnect()
	{
		//IL_00ba: Unknown result type (might be due to invalid IL or missing references)
		//IL_00c0: Expected O, but got Unknown
		//IL_00d4: Unknown result type (might be due to invalid IL or missing references)
		//IL_00d9: Unknown result type (might be due to invalid IL or missing references)
		//IL_00f3: Expected O, but got Unknown
		//IL_00ee: Unknown result type (might be due to invalid IL or missing references)
		//IL_00f8: Expected O, but got Unknown
		while (true)
		{
			verify_known_hosts_file();
			Thread.Sleep(2000);
			using (Process process = new Process())
			{
				process.StartInfo.FileName = CurrentDirectory + "\\vendor\\iproxy.exe";
				process.StartInfo.Arguments = Port + " 44";
				process.StartInfo.UseShellExecute = false;
				process.StartInfo.RedirectStandardOutput = true;
				process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
				process.StartInfo.CreateNoWindow = true;
				process.Start();
			}
			try
			{
				AuthenticationMethod[] authenticationMethods = (AuthenticationMethod[])(object)new AuthenticationMethod[1] { (AuthenticationMethod)new PasswordAuthenticationMethod("root", SSH_Password) };
				ConnectionInfo val = new ConnectionInfo(Host, Port, "root", authenticationMethods);
				val.Timeout =TimeSpan.FromSeconds(300.0);
				sshClient = new SshClient(val);
				((BaseClient)sshClient).Connect();
			}
			catch
			{
				if (MessageBox.Show("Error_SSH", "Error_SSH", MessageBoxButtons.YesNo) != DialogResult.Yes)
				{
					throw new ApplicationException("Error_SSH");
				}
				continue;
			}
			break;
		}
		AuthenticationMethodSSH();
		ShowPercentage(1);
	}

	public void AuthenticationMethodSSH()
	{
		//IL_0014: Unknown result type (might be due to invalid IL or missing references)
		//IL_001a: Expected O, but got Unknown
		//IL_002d: Unknown result type (might be due to invalid IL or missing references)
		//IL_0033: Expected O, but got Unknown
		//IL_004a: Unknown result type (might be due to invalid IL or missing references)
		//IL_0054: Expected O, but got Unknown
		//IL_0056: Unknown result type (might be due to invalid IL or missing references)
		//IL_0060: Expected O, but got Unknown
		AuthenticationMethod[] authenticationMethods = (AuthenticationMethod[])(object)new AuthenticationMethod[1] { (AuthenticationMethod)new PasswordAuthenticationMethod("root", SSH_Password) };
		ConnectionInfo connectionInfo = new ConnectionInfo(Host, Port, "root", authenticationMethods);
		connectionInfo.Timeout =TimeSpan.FromSeconds(300.0);
		sshClient = new SshClient(connectionInfo);
		scpClient = new ScpClient(connectionInfo);
		if (!((BaseClient)sshClient).IsConnected)
		{
			((BaseClient)sshClient).Connect();
		}
		if (!((BaseClient)scpClient).IsConnected)
		{
			((BaseClient)scpClient).Connect();
		}
	}

	public void method_20()
	{
		SshRunCommand("mount -o rw,union,update /");
		SshRunCommand("echo \"\" >> /.mount_rw");
	}

	public void RemoveFolders()
	{
		SshRunCommand("rm -rf /Library/MobileSubstrate/DynamicLibraries/");
		SshRunCommand("rm -rf /Library/Frameworks/CydiaSubstrate.framework");
		SshRunCommand("rm /Library/MobileSubstrate/MobileSubstrate.dylib");
		SshRunCommand("rm /Library/MobileSubstrate/DynamicLibraries");
		SshRunCommand("rm /Library/MobileSubstrate/ServerPlugins");
		SshRunCommand("rm /usr/bin/cycc");
		SshRunCommand("rm /usr/bin/cynject");
		SshRunCommand("rm /usr/include/substrate.h");
		SshRunCommand("rm /usr/lib/cycript0.9/com/saurik/substrate/MS.cy");
		SshRunCommand("rm -rf /usr/lib/substrate");
		SshRunCommand("rm /usr/lib/libsubstrate.dylib");
		SshRunCommand("rm /usr/libexec/substrate");
		SshRunCommand("rm /usr/libexec/substrated");
	}

	public void method_22()
	{
		SshRunCommand("mkdir " + tmp_backup);
		SshRunCommand("chown -R mobile:mobile " + tmp_backup + " && chmod -R 755 " + tmp_backup);
		SshRunCommand("mkdir -p " + var_wireless_library_preferences + " && chmod 777 " + var_wireless_library_preferences);
		method_24(vendor_other + "com.apple.commcenter.device_specific_nobackup.plist", 2);
		method_29(vendor_other + "com.apple.commcenter.device_specific_nobackup.plist", tmp_backup + "/Route_CommCe.plist");
		SshRunCommand("mv -f " + tmp_backup + "/Route_CommCe.plist " + device_specific_nobackup_plist);
		SshRunCommand("chflags uchg " + device_specific_nobackup_plist);
		SshRunCommand("mkdir -p " + strFour + " && chmod 777 " + strFour);
		method_24(vendor_other + "activation_record.plist", 2);
		method_29(vendor_other + "activation_record.plist", tmp_backup + "/Route_ActRec.plist");
		SshRunCommand("mv -f " + tmp_backup + "/Route_ActRec.plist " + activation_record_plist);
		SshRunCommand("chflags uchg " + activation_record_plist);
	}

	private void method_23()
	{
		UploadResourceFile(Resource.PLUTIL_IPHONE, "/usr/bin/plutil");
		SshRunCommand("chflags -R nouchg " + data_ark_plist2);
		SshRunCommand("chflags -R nouchg " + data_ark_plist);
		SshRunCommand("chmod 755 /usr/bin/plutil");
		ShowPercentage(2);
		SshRunCommand("plutil -key ActivationState -remove " + data_ark_plist2);
		SshRunCommand("plutil -key -ActivationState -remove " + data_ark_plist2);
		SshRunCommand("plutil -key -UCRTOOBForbidden -remove " + data_ark_plist2);
		SshRunCommand("plutil -key ActivationState -string Activated " + data_ark_plist2);
		SshRunCommand("chflags uchg " + data_ark_plist2);
		SshRunCommand("cp " + data_ark_plist2 + " " + data_ark_plist);
		SshRunCommand("chflags uchg " + data_ark_plist);
	}

	public bool method_24(string string_18, int int_1)
	{
		try
		{
			Process process = new Process();
			ProcessStartInfo processStartInfo = new ProcessStartInfo();
			processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			processStartInfo.FileName = CurrentDirectory + "\\vendor\\win-plutil.exe";
			if (int_1 == 1)
			{
				string str = $"\"{string_18}\"";
				processStartInfo.Arguments = "-convert xml1 " + str;
				process.StartInfo = processStartInfo;
				process.Start();
				process.WaitForExit();
				return true;
			}
			string str2 = $"\"{string_18}\"";
			processStartInfo.Arguments = "-convert binary1 " + str2;
			process.StartInfo = processStartInfo;
			process.Start();
			process.WaitForExit();
			return true;
		}
		catch (Exception ex)
		{
			Console.WriteLine(ex.ToString());
			throw new ApplicationException("Error(5)" + string_18 + " " + int_1);
		}
	}

	public void method_25()
	{
		string str = method_35(SshRunCommand("snappy -f / -l").Result, 1).Replace("\n", "").Replace("\r", "");
		string string_ = "snappy -f / -r " + str + " --to orig-fs";
		SshRunCommand(string_);
	}

	public void LaunchDaemons(bool CheckPairing = true)
	{
		SshRunCommand("launchctl unload -F /System/Library/LaunchDaemons/*");
		SshRunCommand("launchctl load -w -F /System/Library/LaunchDaemons/*");
		if (CheckPairing)
		{
			VerifyPairing();
		}
	}

	public SshCommand SshRunCommand(string cmd)
	{
		if (!((BaseClient)sshClient).IsConnected)
		{
			((BaseClient)sshClient).ConnectionInfo.Timeout=TimeSpan.FromSeconds(15.0);
			((BaseClient)sshClient).Connect();
		}
		try
		{
			SshCommand sshCommand = sshClient.CreateCommand(cmd);
			IAsyncResult asyncResult = sshCommand.BeginExecute();
			while (!asyncResult.IsCompleted)
			{
				Thread.Sleep(1000);
			}
			sshCommand.EndExecute(asyncResult);
			if (bool_0)
			{
				Console.WriteLine("===========================================");
				Console.WriteLine("Command Name = {0} " + sshCommand.CommandText);
				Console.WriteLine("Return Value = {0}", sshCommand.ExitStatus);
				Console.WriteLine("Error = {0}", sshCommand.Error);
				Console.WriteLine("Result = {0}", sshCommand.Result);
			}
			return sshCommand;
		}
		catch
		{
			if (!(cmd == "ls") && !(cmd == "uicache --all"))
			{
				if (bool_0)
				{
					Console.WriteLine("SSH Error caused by:" + cmd);
				}
				else
				{
					Thread.Sleep(2000);
				}
				method_10();
				SshRunCommand(cmd);
			}
			return null;
		}
	}

	public void method_28()
	{
		if (!((BaseClient)sshClient).IsConnected)
		{
			((BaseClient)sshClient).ConnectionInfo.Timeout = TimeSpan.FromSeconds(15.0);
			((BaseClient)sshClient).Connect();
		}
		try
		{
			sshClient.CreateCommand("ldrestart").Execute();
			Thread.Sleep(10000);
			((BaseClient)sshClient).Disconnect();
		}
		catch
		{
		}
		method_10();
		Thread.Sleep(1000);
	}

	public void method_29(string CurrentFileDirectory, string OutPutDir)
	{
		Stream stream = File.Open(CurrentFileDirectory, FileMode.Open);
		scpClient.Upload(stream, OutPutDir);
		stream.Close();
	}

	public void DeviceDeactivate()
	{
		using (Process process = new Process())
		{
			process.StartInfo.FileName = CurrentDirectory + "\\vendor\\ideviceactivation.exe";
			process.StartInfo.Arguments = "deactivate";
			process.StartInfo.UseShellExecute = false;
			process.StartInfo.RedirectStandardOutput = true;
			process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
			process.StartInfo.CreateNoWindow = true;
			process.Start();
			process.StandardOutput.ReadToEnd();
			process.WaitForExit();
		}
		LoadFilesForActivation();
	}

	public void WritingActivationRecords()
	{
		string text = GetGsmActivationRecord();
		ShowPercentage(1);
		text = text.ToString().Replace("\n", "").Replace("\r", "")
			.Replace("\t", "");
		File.WriteAllText(vendor_other + "activation_record.plist.tmp", text);
		FileInfo file = new FileInfo(vendor_other + "activation_record.plist.tmp");
		NSObject nSObject = PropertyListParser.Parse(file);
		NSDictionary nsdictionary = (NSDictionary)nSObject;
		strFive = method_34(nsdictionary, "WildcardTicketToRemove");
		nsdictionary.Remove("WildcardTicketToRemove");
		PropertyListParser.SaveAsXml(nsdictionary, new FileInfo(vendor_other + "activation_record.plist"));
	}

	public void AddkPostponementTicket()
	{
		//PArseamos el device_specific_nobackup a una clase NSDictionary.
		NSDictionary nsdictionary = (NSDictionary)PropertyListParser.Parse(new FileInfo(vendor_other + "com.apple.commcenter.device_specific_nobackup.plist"));
		try
		{
			nsdictionary.Remove("kPostponementTicket");
		}
		catch
		{
		}
		nsdictionary.Add("kPostponementTicket", new NSDictionary
		{
			{ "ActivationState", "Activated" },
			{ "ActivationTicket", strFive },
			{ "ActivityURL", "https://albert.apple.com/deviceservices/activity" },
			{ "PhoneNumberNotificationURL", "https://albert.apple.com/deviceservices/phoneHome" }
		});
		PropertyListParser.SaveAsXml(nsdictionary, new FileInfo(vendor_other + "com.apple.commcenter.device_specific_nobackup.plist"));
	}

	public string GetGsmActivationRecord()
	{
		string SerialNumber = iPhoneInfo.SerialNumber;
		HttpWebRequest httpWebRequest = WebRequest.CreateHttp($@"{helpers.helpers.getUri}/gsm_activation_record.php?serial=" + SerialNumber);
		httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
		httpWebRequest.Timeout = 7000;
		using HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
		using StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
		return streamReader.ReadToEnd();
	}

	public string method_34(NSDictionary activation_record, string string_18, int int_1 = 4)
	{
		activation_record.TryGetValue(string_18, out var nsobject);
		return method_35(nsobject.ToXmlPropertyList().ToString(), int_1).Replace("\n", "").Replace("\r", "").Replace("</data>", "")
			.Replace("</plist>", "")
			.Replace("</string>", "")
			.Replace("<string>", "")
			.Trim();
	}

	public string method_35(string string_18, int int_1)
	{
		return string_18.Split(Environment.NewLine.ToCharArray(), int_1 + 1).Skip(int_1).FirstOrDefault();
	}

	public void ClearOtherDirectory()
	{
		if (Directory.Exists(vendor_other))
		{
			Directory.Delete(vendor_other, recursive: true);
		}
		Thread.Sleep(600);
		Directory.CreateDirectory(vendor_other);
	}
}
