using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Management;
using System.Net;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using LibUsbDotNet.DeviceNotify;
using MetroFramework;
using MetroFramework.Controls;
using Microsoft.CSharp.RuntimeBinder;
using Renci.SshNet;

public class MainForm : Form
{
    public delegate void GDelegate0(object sender, EventArrivedEventArgs e);

    [CompilerGenerated]
    private static class Class11
    {
        public static CallSite<Func<CallSite, object, object>> callSite_0;

        public static CallSite<Func<CallSite, Type, object, SolidBrush>> callSite_1;

        public static CallSite<Action<CallSite, object>> callSite_2;

        public static CallSite<Action<CallSite, object>> callSite_3;
    }

    [CompilerGenerated]
    private sealed class Class12
    {
        public Action action_0;

        public AutoResetEvent autoResetEvent_0;

        internal void method_0()
        {
            action_0();
            autoResetEvent_0.Set();
        }
    }

    [CompilerGenerated]
    private sealed class Class13
    {
        public string string_0;

        internal void method_0()
        {
            Clipboard.SetText(string_0);
        }
    }

    private bool bool_0 = true;

    public const int int_0 = 161;

    public const int int_1 = 2;

    private static readonly string string_0 = Environment.NewLine;

    private const int int_2 = 65535;

    private const uint uint_0 = 26u;

    private ManagementEventWatcher managementEventWatcher_0;

    private ManagementEventWatcher managementEventWatcher_1;

    public static IDeviceNotifier ideviceNotifier_0 = DeviceNotifier.OpenDeviceNotifier();

    private string string_1 = Directory.GetCurrentDirectory();

    public string string_2 = "";

    public string BuildVersion = "";

    public string ProductType = "";

    private Process iProxyProcess;

    private Process iDeviceInfoProcess;

    public int int_3 = 2222;

    public int int_4 = 44;

    private SshClient sshClient_0;

    private ScpClient scpClient_0;

    private IContainer icontainer_0;

    private BackgroundWorker backgroundWorker_0;

    private Label btnMin;

    private Label btnClose;

    private Panel panel2;

    private Label label6;

    private Label label2;

    private Label label3;

    private Label label10;

    private Label label5;

    private Label label4;

    private ProgressBar progressBar1;

    private Label labelMeid;

    private Label labelUDID;

    private Label label7;

    public Label labelImei;

    private Label labelDebug;

    private Label labelActivationState;

    private Label label12;

    private Label labelSerial;

    private MetroButton btnGSM;

    private Label labelModel1;

    private MetroButton btnMEID;

    private Label labelVersion;

    private Label lblStatus;

    private Panel pnMain;
    private Label label8;
    private Label lblTitle;
    private Label label1;
    private Label label9;
    private PictureBox pictureBox3;

    [DllImport("user32.dll")]
    public static extern int SendMessage(IntPtr intptr_0, int int_5, int int_6, int int_7);

    [DllImport("user32.dll")]
    public static extern bool ReleaseCapture();

    [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
    private static extern bool SendNotifyMessage(IntPtr intptr_0, uint uint_1, UIntPtr uintptr_0, string string_5);

    public MainForm()
    {
        InitializeComponent();
        EventQuery query = new EventQuery("SELECT * FROM __InstanceCreationEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");
        managementEventWatcher_0 = new ManagementEventWatcher(new ManagementScope("root\\CIMV2")
        {
            Options =
            {
                EnablePrivileges = true
            }
        }, query);
        GDelegate0 @object = RunBackgroundJailbrakeStatus;
        managementEventWatcher_0.EventArrived += @object.Invoke;
        managementEventWatcher_0.Start();
        EventQuery query2 = new EventQuery("SELECT * FROM __InstanceDeletionEvent WITHIN 2 WHERE TargetInstance ISA 'Win32_PnPEntity'");
        managementEventWatcher_1 = new ManagementEventWatcher(new ManagementScope("root\\CIMV2")
        {
            Options =
            {
                EnablePrivileges = true
            }
        }, query2);
        managementEventWatcher_1.EventArrived += managementEventWatcher_1_EventArrived;
        managementEventWatcher_1.Start();
        LinkLabel.Link value = new LinkLabel.Link();
        GetJailbrakeStatus();
    }

    private void RunBackgroundJailbrakeStatus(object sender, EventArrivedEventArgs e)
    {
        Control.CheckForIllegalCrossThreadCalls = false;
        GetJailbrakeStatus();
    }

    private void managementEventWatcher_1_EventArrived(object sender, EventArrivedEventArgs e)
    {
        Control.CheckForIllegalCrossThreadCalls = false;
        Control.CheckForIllegalCrossThreadCalls = false;
        ManagementBaseObject managementBaseObject = (ManagementBaseObject)e.NewEvent.Properties["TargetInstance"].Value;
        string text = (string)managementBaseObject.Properties["Description"].Value;
        string text2 = (string)managementBaseObject.Properties["DeviceID"].Value;
        string text3 = (string)managementBaseObject.Properties["Status"].Value;
        if (text.Contains("Apple"))
        {
            lblStatus.Text = "Sin Jailbreak";
            ((Control)(object)btnGSM).Enabled = false;
            pnMain.Visible = false;
        }
    }

    private void GetJailbrakeStatus()
    {
        if (AllowAndGetDeviceInfo())
        {
            lblStatus.Text = "Jailbreak Exitoso";
            lblStatus.ForeColor = Color.Green;
            ((Control)(object)btnGSM).Enabled = true;
            pnMain.Visible = true;
            AllowAndGetDeviceInfo();
        }
        else
        {
            lblStatus.Text = "Porfavor Conecta el Dispositivo con Jailbreak";
            lblStatus.ForeColor = Color.Red;
            ((Control)(object)btnGSM).Enabled = false;
            pnMain.Visible = false;
        }
    }

    private bool AllowAndGetDeviceInfo(string args = "")
    {
        Control.CheckForIllegalCrossThreadCalls = false;
        iDeviceInfoProcess = new Process();
        iDeviceInfoProcess.StartInfo.FileName = Environment.CurrentDirectory + "/vendor/ideviceinfo.exe";
        iDeviceInfoProcess.StartInfo.Arguments = args;
        iDeviceInfoProcess.StartInfo.UseShellExecute = false;
        iDeviceInfoProcess.StartInfo.RedirectStandardOutput = true;
        iDeviceInfoProcess.StartInfo.CreateNoWindow = true;
        iDeviceInfoProcess.Start();
        iPhoneInfo.MEID = false;
        int num = 0;
        while (!iDeviceInfoProcess.StandardOutput.EndOfStream)
        {
            num++;
            string result = iDeviceInfoProcess.StandardOutput.ReadLine();
            string strDeviceInfo = result.Replace("\r", "");
            if (strDeviceInfo.StartsWith("UniqueDeviceID: "))
            {
                string UniqueDeviceID = strDeviceInfo.Replace("UniqueDeviceID: ", "");
                labelUDID.Text = UniqueDeviceID;
            }
            else if (strDeviceInfo.StartsWith("ProductVersion: "))
            {
                string ProductVersion = (string_2 = strDeviceInfo.Replace("ProductVersion: ", ""));
                labelVersion.Text = ProductVersion;
                iPhoneInfo.ProductVersion = ProductVersion;
            }
            else if (strDeviceInfo.StartsWith("BuildVersion: "))
            {
                BuildVersion = strDeviceInfo.Replace("BuildVersion: ", "");
            }
            else if (strDeviceInfo.StartsWith("DeviceName: "))
            {
                strDeviceInfo.Replace("DeviceName: ", "");
            }
            else if (strDeviceInfo.StartsWith("ProductType: "))
            {
                ProductType = strDeviceInfo.Replace("ProductType: ", "");
                labelModel1.Text = ProductType;
            }
            else if (strDeviceInfo.StartsWith("DeviceClass: "))
            {
                strDeviceInfo.Replace("DeviceClass: ", "");
            }
            else if (strDeviceInfo.StartsWith("DeviceColor: "))
            {
                strDeviceInfo.Replace("DeviceColor: ", "");
            }
            else if (strDeviceInfo.StartsWith("HardwareModel: "))
            {
                strDeviceInfo.Replace("HardwareModel: ", "");
            }
            else if (strDeviceInfo.StartsWith("ProductName: "))
            {
                strDeviceInfo.Replace("ProductName: ", "").ToLower();
            }
            else if (strDeviceInfo.StartsWith("TelephonyCapability: "))
            {
                strDeviceInfo.Replace("TelephonyCapability: ", "");
            }
            string[] infoArray = result.Split(':');
            if (infoArray[0] == "SerialNumber")
            {
                iPhoneInfo.SerialNumber = infoArray[1].Trim();
                labelSerial.Text = infoArray[1].Trim();
            }
            if (infoArray[0] == "InternationalMobileEquipmentIdentity")
            {
                iPhoneInfo.IMEI = infoArray[1].Trim();
                labelImei.Text = infoArray[1].Trim();
            }
            if (infoArray[0] == "ActivationState")
            {
                iPhoneInfo.ActivationState = infoArray[1].Trim();
                labelActivationState.Text = infoArray[1].Trim();
            }
            if (infoArray[0] == "SIMStatus")
            {
                iPhoneInfo.SIMStatus = infoArray[1].Trim();
                iPhoneInfo.SimCardAvailable = (iPhoneInfo.SIMStatus == "kCTSIMSupportSIMStatusReady") ^ (iPhoneInfo.SIMStatus == "kCTSIMSupportSIMStatusPINLocked");
            }
            if (result.Contains("MobileEquipmentIdentifier"))
            {
                iPhoneInfo.MEID = true;
            }
            if (result.Contains("UniqueDeviceID"))
            {
                labelUDID.Text = infoArray[1];
            }
        }
        if (iPhoneInfo.MEID)
        {
            labelMeid.Text = "MEID";
            ((Control)(object)btnGSM).Enabled = false;
            ((Control)(object)btnMEID).Enabled = true;
        }
        else
        {
            labelMeid.Text = "GSM";
            ((Control)(object)btnGSM).Enabled = true;
            ((Control)(object)btnMEID).Enabled = false;
        }
        return num > 2;
    }

    private void CheckUSBConnect(object sender, DeviceNotifyEventArgs e)
    {
        if (!(e.EventType.ToString() == "DeviceRemoveComplete"))
        {
            lblStatus.Text = "iDevice Information";
            ((Control)(object)btnGSM).Enabled = true;
            pnMain.Visible = false;
            GetJailbrakeStatus();
        }
    }

    private void MainForm_Load(object sender, EventArgs e)
    {
        ideviceNotifier_0.OnDeviceNotify += CheckUSBConnect;
        string caption = "Double click to copy";
        new ToolTip().SetToolTip(labelImei, caption);
        new ToolTip().SetToolTip(labelSerial, caption);
        new ToolTip().SetToolTip(labelUDID, caption);
        new ToolTip().SetToolTip(labelModel1, caption);
        new ToolTip().SetToolTip(labelVersion, caption);
    }

    private void method_4(object sender, LinkLabelLinkClickedEventArgs e)
    {
    }

    private void buttonBypass_Click(object sender, EventArgs e)
    {
        RunBypassGsmFull();
    }

    private void backgroundWorker_0_DoWork(object sender, DoWorkEventArgs e)
    {
        Control.CheckForIllegalCrossThreadCalls = false;
        ((Control)(object)btnGSM).Enabled = false;
        try
        {
            RemoveIproxyProcess();
        }
        catch (Exception)
        {
        }
        try
        {
            //string Status_SN_In_Server = GetStatusSerialNumber();
            //Status_SN_In_Server = Status_SN_In_Server.ToString().Replace("\n", "").Replace("\r", "")
            //    .Replace("\t", "");
            //num = (!iPhoneInfo.bool_1) ? 1 : 0;
            //if (num != 0)
            //{
            //	RunBypassGsmFull();
            //}
            //if (Status_SN_In_Server == "AUTHORIZED" && iPhoneInfo.bool_1)
            //{
            //	RunBypassMEID();
            //}
            //method_10(0, "EXITO");

            string Status_SN_In_Server = "AUTHORIZED";
            int num;
            switch (Status_SN_In_Server)
            {
                case "AUTHORIZED":
                    num = (!iPhoneInfo.MEID) ? 1 : 0;
                    goto Registered;
                default:
                    num = 0;
                    goto Registered;
                case "NOT_AUTHORIZED":
                    MessageBox.Show("Sorry Your iDevice Serial Not Registered", "SN Check", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                    SetPercentaje(0, "Not Authorized Serial");
                    ((Control)(object)btnGSM).Enabled = true;
                    break;
                case "CONECTION_ERROR":
                    {
                        MessageBox.Show("No internet Connection", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Hand);
                        SetPercentaje(0, "NO INTERNET");
                        ((Control)(object)btnGSM).Enabled = true;
                        break;
                    }
                Registered:
                    if (num != 0)
                    {
                        RunBypassGsmFull();
                    }
                    if (Status_SN_In_Server == "AUTHORIZED" && iPhoneInfo.MEID)
                    {
                        RunBypassMEID();
                    }
                    SetPercentaje(0, "EXITO");
                    break;
            }

        }
        catch (Exception value)
        {
            SetPercentaje(0, "ERROR");
            Console.WriteLine(value);
            MessageBox.Show("Close this software and try again", "[IMPORTANT]", MessageBoxButtons.OK, MessageBoxIcon.Hand);
        }
        finally
        {
            Invoke((MethodInvoker)delegate
            {
                ((Control)(object)btnGSM).Text = "Activando GSM Full....";
            });
            ((Control)(object)btnGSM).Enabled = true;
        }
    }

    public void RunBypassGsmFull()
    {
        Invoke((MethodInvoker)delegate
        {
            ((Control)(object)btnGSM).Text = "Bypass GSM Full";
        });
        Invoke((MethodInvoker)delegate
        {
            ((Control)(object)btnGSM).Enabled = false;
        });
        if (iPhoneInfo.SimCardAvailable)
        {
            MessageBox.Show("Tarjeta SIM no detectada, se recomienda INSERTAR la SIM ", " Tarjeta SIM no detectada", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
        }
        new iCloudByPass().BypassGSM();
    }

    public void RunBypassMEID()
    {
        Invoke((MethodInvoker)delegate
        {
            ((Control)(object)btnGSM).Text = "Activando Meid Full....";
        });
        Invoke((MethodInvoker)delegate
        {
            ((Control)(object)btnGSM).Enabled = false;
        });
        if (!iPhoneInfo.SimCardAvailable)
        {
            MessageBox.Show("Tarjeta SIM no detectada, se recomienda INSERTAR la SIM ", " Tarjeta SIM no detectada", MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);
        }
        new iCloudByPass().BypassMEID();
    }

    private void RemoveIproxyProcess()
    {
        Process[] processesByName = Process.GetProcessesByName("iproxy");
        if (processesByName.Length >= 1)
        {
            Process[] array = processesByName;
            for (int i = 0; i < array.Length; i++)
            {
                array[i].Kill();
            }
            iProxyProcess = null;
        }
    }

    public void RuniProxyProcess(int int_5, int int_6)
    {
        string text = Environment.CurrentDirectory + "/vendor/iproxy.exe";
        if ((iProxyProcess == null && File.Exists(text)) || iProxyProcess.HasExited)
        {
            iProxyProcess = new Process();
            iProxyProcess.StartInfo.FileName = text;
            iProxyProcess.StartInfo.Arguments = int_5 + " " + int_6;
            iProxyProcess.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            iProxyProcess.Start();
        }
        else
        {
            File.Exists(text);
        }
    }

    //public void method_9(string string_5, int int_5, string string_6)
    //{
    //    //IL_000f: Unknown result type (might be due to invalid IL or missing references)
    //    //IL_0015: Expected O, but got Unknown
    //    //IL_001e: Unknown result type (might be due to invalid IL or missing references)
    //    //IL_0028: Expected O, but got Unknown
    //    //IL_0023: Unknown result type (might be due to invalid IL or missing references)
    //    //IL_0029: Expected O, but got Unknown
    //    AuthenticationMethod[] authenticationMethods = (AuthenticationMethod[])(object)new AuthenticationMethod[1] { (AuthenticationMethod)new PasswordAuthenticationMethod("root", string_6) };
    //    SshClient sshClient = new SshClient(new ConnectionInfo(string_5, int_5, "root", authenticationMethods));
    //    ((BaseClient)sshClient).Connect();
    //    ((BaseClient)sshClient).Disconnect();
    //}

    private void backgroundWorker_0_ProgressChanged(object sender, ProgressChangedEventArgs e)
    {
        Control.CheckForIllegalCrossThreadCalls = false;
        progressBar1.Value = e.ProgressPercentage;
    }

    private void backgroundWorker_0_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
    {
        Control.CheckForIllegalCrossThreadCalls = false;
        progressBar1.Value = 0;
        ((Control)(object)btnGSM).Enabled = true;
    }

    public void SetPercentaje(int percentaje, string msg)
    {
        backgroundWorker_0.ReportProgress(percentaje);
        labelDebug.Text = percentaje + "%";
        if (bool_0)
        {
            Console.WriteLine(msg);
        }
    }

    private void buttonBypass_EnabledChanged(object sender, EventArgs e)
    {
        ((Control)(object)btnGSM).ForeColor = Color.White;
    }

    private void buttonBypass_Paint(object sender, PaintEventArgs e)
    {
        dynamic arg = (Button)sender;
        if (Class11.callSite_1 == null)
        {
            Class11.callSite_1 = CallSite<Func<CallSite, Type, object, SolidBrush>>.Create(Binder.InvokeConstructor(CSharpBinderFlags.None, typeof(MainForm), new CSharpArgumentInfo[2]
            {
                CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.UseCompileTimeType | CSharpArgumentInfoFlags.IsStaticType, null),
                CSharpArgumentInfo.Create(CSharpArgumentInfoFlags.None, null)
            }));
        }
        Func<CallSite, Type, object, SolidBrush> target = Class11.callSite_1.Target;
        CallSite callSite_ = Class11.callSite_1;
        Type typeFromHandle = typeof(SolidBrush);
        dynamic arg2 = target(callSite_, typeFromHandle, (object)arg.ForeColor);
        dynamic arg3 = new StringFormat
        {
            Alignment = StringAlignment.Center,
            LineAlignment = StringAlignment.Center
        };
        arg2.Dispose();
        arg3.Dispose();
    }

    private void btnClose_Click(object sender, EventArgs e)
    {
        try
        {
            if (iProxyProcess != null)
            {
                iProxyProcess.Kill();
                iProxyProcess = null;
            }
            RemoveIproxyProcess();
        }
        catch (Exception)
        {
        }
        Environment.Exit(0);
    }

    private void btnMin_Click(object sender, EventArgs e)
    {
        base.WindowState = FormWindowState.Minimized;
    }

    private void pbWait_Click(object sender, EventArgs e)
    {
    }

    private void label1_MouseDown(object sender, MouseEventArgs e)
    {
    }

    public string GetStatusSerialNumber()
    {
        string SerialNumber = iPhoneInfo.SerialNumber;
        HttpWebRequest httpWebRequest = WebRequest.CreateHttp("http://netxunlock.com/Auth/burtgelshalgah.php?serialNumber=" + SerialNumber);
        httpWebRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
        httpWebRequest.Timeout = 7000;
        using HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
        using StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
        return streamReader.ReadToEnd();
    }

    private static void smethod_0(Action action_0)
    {
        AutoResetEvent autoResetEvent_0 = new AutoResetEvent(initialState: false);
        Thread thread = new Thread((ThreadStart)delegate
        {
            action_0();
            autoResetEvent_0.Set();
        });
        thread.SetApartmentState(ApartmentState.STA);
        thread.Start();
        autoResetEvent_0.WaitOne();
    }

    private void button1_Click(object sender, EventArgs e)
    {
        string string_0 = labelSerial.Text;
        smethod_0(delegate
        {
            Clipboard.SetText(string_0);
        });
    }

    private void label1_Click(object sender, EventArgs e)
    {
    }

    private void method_15(object sender, EventArgs e)
    {
    }

    private void method_16(object sender, EventArgs e)
    {
    }

    private void pictureBox3_Click(object sender, EventArgs e)
    {
    }

    private void linkLabelTwiter_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
    {
    }

    private void label7_Click(object sender, EventArgs e)
    {
    }

    private void pictureBox4_Click(object sender, EventArgs e)
    {
    }

    private void labelSerial_Click(object sender, EventArgs e)
    {
    }

    protected new virtual void Dispose(bool disposing)
    {
        if (disposing && icontainer_0 != null)
        {
            icontainer_0.Dispose();
        }
        base.Dispose(disposing);
    }

    [CompilerGenerated]
    private void MessageActivandoGSM()
    {
        ((Control)(object)btnGSM).Text = "Activando GSM Full....";
    }

    [CompilerGenerated]
    private void MesssageBypassGMS()
    {
        ((Control)(object)btnGSM).Text = "Bypass GSM Full";
    }

    [CompilerGenerated]
    private void DisableButtonGSMBypass()
    {
        ((Control)(object)btnMEID).Enabled = false;
    }

    [CompilerGenerated]
    private void MessageActivandoMEID()
    {
        ((Control)(object)btnMEID).Text = "Activando Meid Full....";
    }

    [CompilerGenerated]
    private void method_21()
    {
        ((Control)(object)btnGSM).Enabled = false;
    }

    private void Button1_Click_1(object sender, EventArgs e)
    {
        RunBypassMEID();
    }

    private void btnGSM_Click(object sender, EventArgs e)
    {
        backgroundWorker_0.RunWorkerAsync();
    }

    private void btnMEID_Click(object sender, EventArgs e)
    {
        RunBypassMEID();
    }

    private void Panel2_Paint(object sender, PaintEventArgs e)
    {
    }

    private void groupBox1_Enter(object sender, EventArgs e)
    {
    }

    private void lblStatus_Click(object sender, EventArgs e)
    {
    }

    private void labelUDID_Click(object sender, EventArgs e)
    {
    }

    private void labelMeid_Click(object sender, EventArgs e)
    {
    }

    private void labelActivationState_Click(object sender, EventArgs e)
    {
    }

    private void labelModel1_Click(object sender, EventArgs e)
    {
    }

    private void pnMain_Paint(object sender, PaintEventArgs e)
    {
    }

    private void label5_Click(object sender, EventArgs e)
    {
    }

    private void label3_Click(object sender, EventArgs e)
    {
    }

    private void label2_Click(object sender, EventArgs e)
    {
    }

    private void label6_Click(object sender, EventArgs e)
    {
    }

    private void label12_Click(object sender, EventArgs e)
    {
    }

    private void label10_Click(object sender, EventArgs e)
    {
    }

    private void label4_Click(object sender, EventArgs e)
    {
    }

    private void label7_Click_1(object sender, EventArgs e)
    {
    }

    private void labelSerial_Click_1(object sender, EventArgs e)
    {
    }

    private void labelImei_Click(object sender, EventArgs e)
    {
    }

    private void labelVersion_Click(object sender, EventArgs e)
    {
    }

    private void btnMin_Click_1(object sender, EventArgs e)
    {
        base.WindowState = FormWindowState.Minimized;
    }

    private void pictureBox2_Click(object sender, EventArgs e)
    {
    }

    private void btnClose_Click_1(object sender, EventArgs e)
    {
        Close();
    }

    private void pictureBox3_Click_1(object sender, EventArgs e)
    {
    }

    private void pictureBox5_Click(object sender, EventArgs e)
    {
        Process.Start("https://www.facebook.com/Sethos2.2");
    }

    private void pictureBox1_Click(object sender, EventArgs e)
    {
    }

    private void pnMain_Paint_1(object sender, PaintEventArgs e)
    {
    }

    private void label10_Click_1(object sender, EventArgs e)
    {
    }

    private void InitializeComponent()
    {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.backgroundWorker_0 = new System.ComponentModel.BackgroundWorker();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.labelMeid = new System.Windows.Forms.Label();
            this.labelUDID = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.labelImei = new System.Windows.Forms.Label();
            this.labelDebug = new System.Windows.Forms.Label();
            this.labelActivationState = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.labelSerial = new System.Windows.Forms.Label();
            this.btnGSM = new MetroFramework.Controls.MetroButton();
            this.labelModel1 = new System.Windows.Forms.Label();
            this.btnMEID = new MetroFramework.Controls.MetroButton();
            this.labelVersion = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.pnMain = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label8 = new System.Windows.Forms.Label();
            this.lblTitle = new System.Windows.Forms.Label();
            this.btnClose = new System.Windows.Forms.Label();
            this.btnMin = new System.Windows.Forms.Label();
            this.pnMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // backgroundWorker_0
            // 
            this.backgroundWorker_0.WorkerReportsProgress = true;
            this.backgroundWorker_0.WorkerSupportsCancellation = true;
            this.backgroundWorker_0.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker_0_DoWork);
            this.backgroundWorker_0.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker_0_ProgressChanged);
            this.backgroundWorker_0.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker_0_RunWorkerCompleted);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.OrangeRed;
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Impact", 12F);
            this.label6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label6.Location = new System.Drawing.Point(24, 105);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label6.Size = new System.Drawing.Size(53, 22);
            this.label6.TabIndex = 16;
            this.label6.Text = "Model:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.OrangeRed;
            this.label2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Impact", 12F);
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(24, 75);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(52, 22);
            this.label2.TabIndex = 11;
            this.label2.Text = "Serial:";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.OrangeRed;
            this.label3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Impact", 12F);
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(24, 44);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label3.Size = new System.Drawing.Size(42, 22);
            this.label3.TabIndex = 9;
            this.label3.Text = "IMEI:";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.OrangeRed;
            this.label10.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label10.Font = new System.Drawing.Font("Impact", 12F);
            this.label10.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label10.Location = new System.Drawing.Point(23, 173);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(90, 22);
            this.label10.TabIndex = 41;
            this.label10.Text = "Meid Status:";
            this.label10.Click += new System.EventHandler(this.label10_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.OrangeRed;
            this.label5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label5.Font = new System.Drawing.Font("Impact", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label5.Location = new System.Drawing.Point(24, 13);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label5.Size = new System.Drawing.Size(87, 22);
            this.label5.TabIndex = 14;
            this.label5.Text = "iOS Version:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.OrangeRed;
            this.label4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label4.Font = new System.Drawing.Font("Impact", 12F);
            this.label4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label4.Location = new System.Drawing.Point(24, 206);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label4.Size = new System.Drawing.Size(43, 22);
            this.label4.TabIndex = 12;
            this.label4.Text = "Udid:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.BackColor = System.Drawing.Color.DarkGray;
            this.progressBar1.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            this.progressBar1.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.progressBar1.Location = new System.Drawing.Point(22, 372);
            this.progressBar1.Margin = new System.Windows.Forms.Padding(2);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(670, 22);
            this.progressBar1.TabIndex = 20;
            // 
            // labelMeid
            // 
            this.labelMeid.AutoSize = true;
            this.labelMeid.BackColor = System.Drawing.Color.Transparent;
            this.labelMeid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelMeid.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMeid.ForeColor = System.Drawing.Color.Black;
            this.labelMeid.Location = new System.Drawing.Point(152, 173);
            this.labelMeid.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelMeid.Name = "labelMeid";
            this.labelMeid.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelMeid.Size = new System.Drawing.Size(37, 21);
            this.labelMeid.TabIndex = 40;
            this.labelMeid.Text = "N/A";
            this.labelMeid.Click += new System.EventHandler(this.labelMeid_Click);
            // 
            // labelUDID
            // 
            this.labelUDID.AutoSize = true;
            this.labelUDID.BackColor = System.Drawing.Color.Transparent;
            this.labelUDID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelUDID.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.labelUDID.Font = new System.Drawing.Font("Segoe UI Semibold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUDID.ForeColor = System.Drawing.Color.Black;
            this.labelUDID.Location = new System.Drawing.Point(152, 206);
            this.labelUDID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelUDID.Name = "labelUDID";
            this.labelUDID.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelUDID.Size = new System.Drawing.Size(31, 17);
            this.labelUDID.TabIndex = 13;
            this.labelUDID.Text = "N/A";
            this.labelUDID.Click += new System.EventHandler(this.labelUDID_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.OrangeRed;
            this.label7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label7.Font = new System.Drawing.Font("Impact", 12F);
            this.label7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label7.Location = new System.Drawing.Point(24, 234);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label7.Size = new System.Drawing.Size(123, 22);
            this.label7.TabIndex = 41;
            this.label7.Text = "Jailbreack Status";
            this.label7.Click += new System.EventHandler(this.label7_Click_1);
            // 
            // labelImei
            // 
            this.labelImei.AutoSize = true;
            this.labelImei.BackColor = System.Drawing.Color.Transparent;
            this.labelImei.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelImei.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelImei.ForeColor = System.Drawing.Color.Black;
            this.labelImei.Location = new System.Drawing.Point(152, 44);
            this.labelImei.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelImei.Name = "labelImei";
            this.labelImei.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelImei.Size = new System.Drawing.Size(37, 21);
            this.labelImei.TabIndex = 10;
            this.labelImei.Text = "N/A";
            this.labelImei.Click += new System.EventHandler(this.labelImei_Click);
            // 
            // labelDebug
            // 
            this.labelDebug.AutoSize = true;
            this.labelDebug.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelDebug.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelDebug.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDebug.ForeColor = System.Drawing.Color.Lime;
            this.labelDebug.Location = new System.Drawing.Point(696, 376);
            this.labelDebug.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelDebug.Name = "labelDebug";
            this.labelDebug.Size = new System.Drawing.Size(31, 18);
            this.labelDebug.TabIndex = 21;
            this.labelDebug.Text = "0%";
            // 
            // labelActivationState
            // 
            this.labelActivationState.AutoSize = true;
            this.labelActivationState.BackColor = System.Drawing.Color.Transparent;
            this.labelActivationState.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelActivationState.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelActivationState.ForeColor = System.Drawing.Color.Black;
            this.labelActivationState.Location = new System.Drawing.Point(152, 141);
            this.labelActivationState.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelActivationState.Name = "labelActivationState";
            this.labelActivationState.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelActivationState.Size = new System.Drawing.Size(37, 21);
            this.labelActivationState.TabIndex = 38;
            this.labelActivationState.Text = "N/A";
            this.labelActivationState.Click += new System.EventHandler(this.labelActivationState_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.Color.OrangeRed;
            this.label12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label12.Font = new System.Drawing.Font("Impact", 12F);
            this.label12.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label12.Location = new System.Drawing.Point(24, 141);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label12.Size = new System.Drawing.Size(119, 22);
            this.label12.TabIndex = 43;
            this.label12.Text = "Activation Satus:";
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // labelSerial
            // 
            this.labelSerial.AutoSize = true;
            this.labelSerial.BackColor = System.Drawing.Color.Transparent;
            this.labelSerial.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelSerial.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSerial.ForeColor = System.Drawing.Color.Black;
            this.labelSerial.Location = new System.Drawing.Point(152, 75);
            this.labelSerial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelSerial.Name = "labelSerial";
            this.labelSerial.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelSerial.Size = new System.Drawing.Size(37, 21);
            this.labelSerial.TabIndex = 8;
            this.labelSerial.Text = "N/A";
            this.labelSerial.Click += new System.EventHandler(this.labelSerial_Click_1);
            // 
            // buttonGSMBypass
            // 
            this.btnGSM.BackColor = System.Drawing.Color.OrangeRed;
            this.btnGSM.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnGSM.ForeColor = System.Drawing.Color.White;
            this.btnGSM.Location = new System.Drawing.Point(52, 294);
            this.btnGSM.Name = "buttonGSMBypass";
            this.btnGSM.Size = new System.Drawing.Size(231, 54);
            this.btnGSM.TabIndex = 50;
            this.btnGSM.Text = "GSM Bypass Full";
            this.btnGSM.Theme = MetroFramework.MetroThemeStyle.Light;
            this.btnGSM.UseCustomBackColor = true;
            this.btnGSM.UseCustomForeColor = true;
            this.btnGSM.UseSelectable = true;
            this.btnGSM.Click += new System.EventHandler(this.btnGSM_Click);
            // 
            // labelModel1
            // 
            this.labelModel1.AutoSize = true;
            this.labelModel1.BackColor = System.Drawing.Color.Transparent;
            this.labelModel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelModel1.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModel1.ForeColor = System.Drawing.Color.Black;
            this.labelModel1.Location = new System.Drawing.Point(152, 105);
            this.labelModel1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelModel1.Name = "labelModel1";
            this.labelModel1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelModel1.Size = new System.Drawing.Size(37, 21);
            this.labelModel1.TabIndex = 17;
            this.labelModel1.Text = "N/A";
            this.labelModel1.Click += new System.EventHandler(this.labelModel1_Click);
            // 
            // btnMEID
            // 
            this.btnMEID.BackColor = System.Drawing.Color.OrangeRed;
            this.btnMEID.ForeColor = System.Drawing.Color.White;
            this.btnMEID.Location = new System.Drawing.Point(419, 294);
            this.btnMEID.Name = "btnMEID";
            this.btnMEID.Size = new System.Drawing.Size(231, 54);
            this.btnMEID.Style = MetroFramework.MetroColorStyle.Blue;
            this.btnMEID.TabIndex = 51;
            this.btnMEID.Text = "Meid Bypass  (No Signal)";
            this.btnMEID.Theme = MetroFramework.MetroThemeStyle.Dark;
            this.btnMEID.UseCustomBackColor = true;
            this.btnMEID.UseSelectable = true;
            this.btnMEID.Click += new System.EventHandler(this.btnMEID_Click);
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.BackColor = System.Drawing.Color.Transparent;
            this.labelVersion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelVersion.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVersion.ForeColor = System.Drawing.Color.Black;
            this.labelVersion.Location = new System.Drawing.Point(148, 14);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.labelVersion.Size = new System.Drawing.Size(41, 21);
            this.labelVersion.TabIndex = 15;
            this.labelVersion.Text = "N/A ";
            this.labelVersion.Click += new System.EventHandler(this.labelVersion_Click);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.BackColor = System.Drawing.Color.Transparent;
            this.lblStatus.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblStatus.Font = new System.Drawing.Font("Segoe UI Semibold", 10F, System.Drawing.FontStyle.Bold);
            this.lblStatus.ForeColor = System.Drawing.Color.Black;
            this.lblStatus.Location = new System.Drawing.Point(152, 234);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblStatus.Size = new System.Drawing.Size(37, 21);
            this.lblStatus.TabIndex = 44;
            this.lblStatus.Text = "N/A";
            this.lblStatus.Click += new System.EventHandler(this.lblStatus_Click);
            // 
            // pnMain
            // 
            this.pnMain.BackColor = System.Drawing.Color.Transparent;
            this.pnMain.Controls.Add(this.btnGSM);
            this.pnMain.Controls.Add(this.btnMEID);
            this.pnMain.Controls.Add(this.label9);
            this.pnMain.Controls.Add(this.label1);
            this.pnMain.Controls.Add(this.lblStatus);
            this.pnMain.Controls.Add(this.labelVersion);
            this.pnMain.Controls.Add(this.labelModel1);
            this.pnMain.Controls.Add(this.labelSerial);
            this.pnMain.Controls.Add(this.label12);
            this.pnMain.Controls.Add(this.labelActivationState);
            this.pnMain.Controls.Add(this.labelDebug);
            this.pnMain.Controls.Add(this.labelImei);
            this.pnMain.Controls.Add(this.label7);
            this.pnMain.Controls.Add(this.labelUDID);
            this.pnMain.Controls.Add(this.labelMeid);
            this.pnMain.Controls.Add(this.progressBar1);
            this.pnMain.Controls.Add(this.label4);
            this.pnMain.Controls.Add(this.label5);
            this.pnMain.Controls.Add(this.label10);
            this.pnMain.Controls.Add(this.label3);
            this.pnMain.Controls.Add(this.label2);
            this.pnMain.Controls.Add(this.label6);
            this.pnMain.Controls.Add(this.pictureBox3);
            this.pnMain.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnMain.Location = new System.Drawing.Point(0, 34);
            this.pnMain.Margin = new System.Windows.Forms.Padding(2);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(866, 410);
            this.pnMain.TabIndex = 5;
            this.pnMain.Paint += new System.Windows.Forms.PaintEventHandler(this.pnMain_Paint_1);
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label9.Font = new System.Drawing.Font("Impact", 12F);
            this.label9.ForeColor = System.Drawing.Color.Black;
            this.label9.Location = new System.Drawing.Point(827, 387);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(39, 23);
            this.label9.TabIndex = 28;
            this.label9.Text = "v1.2";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Impact", 10F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(453, 247);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 18);
            this.label1.TabIndex = 53;
            this.label1.Text = "Dev. Ibrah";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::Properties.Resources.background2;
            this.pictureBox3.Image = global::Properties.Resources.background2;
            this.pictureBox3.Location = new System.Drawing.Point(0, 1);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(866, 409);
            this.pictureBox3.TabIndex = 52;
            this.pictureBox3.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.BackgroundImage = global::Properties.Resources.top_bar_full;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.lblTitle);
            this.panel2.Controls.Add(this.btnClose);
            this.panel2.Controls.Add(this.btnMin);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(866, 34);
            this.panel2.TabIndex = 23;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.Panel2_Paint);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe MDL2 Assets", 14F);
            this.label8.ForeColor = System.Drawing.SystemColors.ActiveBorder;
            this.label8.Location = new System.Drawing.Point(64, 5);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 19);
            this.label8.TabIndex = 27;
            this.label8.Text = "iBypass Tool";
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Impact", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(24, 5);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(34, 23);
            this.lblTitle.TabIndex = 26;
            this.lblTitle.Text = "RM";
            // 
            // btnClose
            // 
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.btnClose.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.Color.White;
            this.btnClose.Location = new System.Drawing.Point(835, 9);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(20, 20);
            this.btnClose.TabIndex = 24;
            this.btnClose.Text = "x";
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click_1);
            // 
            // btnMin
            // 
            this.btnMin.BackColor = System.Drawing.Color.Transparent;
            this.btnMin.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.btnMin.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMin.ForeColor = System.Drawing.Color.White;
            this.btnMin.Location = new System.Drawing.Point(800, 5);
            this.btnMin.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.btnMin.Name = "btnMin";
            this.btnMin.Size = new System.Drawing.Size(20, 20);
            this.btnMin.TabIndex = 25;
            this.btnMin.Text = "_";
            this.btnMin.Click += new System.EventHandler(this.btnMin_Click_1);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(866, 444);
            this.Controls.Add(this.pnMain);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RM iBypass Tool";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.pnMain.ResumeLayout(false);
            this.pnMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

    }

    private void pictureBox3_Click_2(object sender, EventArgs e)
    {

    }

    private void pictureBox1_Click_1(object sender, EventArgs e)
    {

    }
}
