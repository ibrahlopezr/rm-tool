using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

[CompilerGenerated]
[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
[DebuggerNonUserCode]
internal class Class15
{
	private static ResourceManager resourceManager_0;

	private static CultureInfo cultureInfo_0;

	[EditorBrowsable(EditorBrowsableState.Advanced)]
	internal static ResourceManager ResourceManager_0
	{
		get
		{
			if (resourceManager_0 == null)
			{
				resourceManager_0 = new ResourceManager("Class15", typeof(Class15).Assembly);
			}
			return resourceManager_0;
		}
	}

	[EditorBrowsable(EditorBrowsableState.Advanced)]
	internal static CultureInfo CultureInfo_0
	{
		get
		{
			return cultureInfo_0;
		}
		set
		{
			cultureInfo_0 = value;
		}
	}

	internal static Bitmap Bitmap_0 => (Bitmap)ResourceManager_0.GetObject("53A15ED1-565E-4D1B-8DA4-1552F2DE14F1-removebg", cultureInfo_0);

	internal static Bitmap Bitmap_1 => (Bitmap)ResourceManager_0.GetObject("bypass", cultureInfo_0);

	internal static Bitmap Bitmap_2 => (Bitmap)ResourceManager_0.GetObject("ELECTROUNLOCK LOGO", cultureInfo_0);

	internal static Bitmap Bitmap_3 => (Bitmap)ResourceManager_0.GetObject("hack", cultureInfo_0);

	internal static Bitmap Bitmap_4 => (Bitmap)ResourceManager_0.GetObject("iExpert", cultureInfo_0);

	internal static Bitmap Bitmap_5 => (Bitmap)ResourceManager_0.GetObject("imeigutus", cultureInfo_0);

	internal static Bitmap Bitmap_6 => (Bitmap)ResourceManager_0.GetObject("IMG-0041", cultureInfo_0);

	internal static Bitmap Bitmap_7 => (Bitmap)ResourceManager_0.GetObject("iphone", cultureInfo_0);

	internal static Bitmap Bitmap_8 => (Bitmap)ResourceManager_0.GetObject("Logo", cultureInfo_0);

	internal static Bitmap Bitmap_9 => (Bitmap)ResourceManager_0.GetObject("LOGO ELECTRO FIX-01", cultureInfo_0);

	internal static Bitmap Bitmap_10 => (Bitmap)ResourceManager_0.GetObject("LOGO ELECTRO FIX-02 (1)", cultureInfo_0);

	internal Class15()
	{
	}
}
