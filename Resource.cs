﻿

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

[CompilerGenerated]
[DebuggerNonUserCode]
[GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
internal class Resource
{
  private static ResourceManager resourceManager_0;
  private static CultureInfo cultureInfo_0;

  [EditorBrowsable(EditorBrowsableState.Advanced)]
  internal static ResourceManager ResourceManager_0
  {
    get
    {
      if (Resource.resourceManager_0 == null)
        Resource.resourceManager_0 = new ResourceManager(nameof (Resource), typeof (Resource).Assembly);
      return Resource.resourceManager_0;
    }
  }

  [EditorBrowsable(EditorBrowsableState.Advanced)]
  internal static CultureInfo CultureInfo_0
  {
    get => Resource.cultureInfo_0;
    set => Resource.cultureInfo_0 = value;
  }

  internal static byte[] AP => (byte[]) Resource.ResourceManager_0.GetObject("ap", Resource.cultureInfo_0);

  internal static byte[] GSM_RAPTOR => (byte[]) Resource.ResourceManager_0.GetObject("gsm_raptor", Resource.cultureInfo_0);

  internal static byte[] IOS12_RESET_PLIST => (byte[]) Resource.ResourceManager_0.GetObject("ios12_Reset_plist", Resource.cultureInfo_0);

  internal static byte[] IOS13_RESET_PLIST => (byte[]) Resource.ResourceManager_0.GetObject("ios13_Reset_plist", Resource.cultureInfo_0);

  internal static byte[] IUNTETHERED => (byte[]) Resource.ResourceManager_0.GetObject("iuntethered", Resource.cultureInfo_0);

  internal static byte[] IUNTETHERED1 => (byte[]) Resource.ResourceManager_0.GetObject("iuntethered1", Resource.cultureInfo_0);

  internal static byte[] LDRUNPS => (byte[]) Resource.ResourceManager_0.GetObject("ldrunps", Resource.cultureInfo_0);

  internal static byte[] LZMA => (byte[]) Resource.ResourceManager_0.GetObject("lzma", Resource.cultureInfo_0);

  internal static byte[] PLUTIL_IPHONE => (byte[]) Resource.ResourceManager_0.GetObject("plutil_iphone", Resource.cultureInfo_0);

  internal static byte[] PURPLEBUDDY => (byte[]) Resource.ResourceManager_0.GetObject("purplebuddy", Resource.cultureInfo_0);

  internal static byte[] RESTORE => (byte[]) Resource.ResourceManager_0.GetObject("restore", Resource.cultureInfo_0);

  internal static byte[] SUBSTRATE_GSM_1 => (byte[]) Resource.ResourceManager_0.GetObject("substrate_gsm_1", Resource.cultureInfo_0);

  internal static byte[] UNTETHERED => (byte[]) Resource.ResourceManager_0.GetObject("untethered", Resource.cultureInfo_0);

  internal static byte[] UNTETHERED1 => (byte[]) Resource.ResourceManager_0.GetObject("untethered1", Resource.cultureInfo_0);

  internal Resource()
  {
  }
}
