﻿

using System.CodeDom.Compiler;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Resources;
using System.Runtime.CompilerServices;

namespace iKingCode_BypassHello.Properties
{
  [GeneratedCode("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
  [DebuggerNonUserCode]
  [CompilerGenerated]
  internal class Class14
  {
    private static ResourceManager resourceMan;
    private static CultureInfo resourceCulture;

    internal Class14()
    {
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static ResourceManager ResourceManager
    {
      get
      {
        if (Class14.resourceMan == null)
          Class14.resourceMan = new ResourceManager("iKingCode_BypassHello.Properties.Class14", typeof (Class14).Assembly);
        return Class14.resourceMan;
      }
    }

    [EditorBrowsable(EditorBrowsableState.Advanced)]
    internal static CultureInfo Culture
    {
      get => Class14.resourceCulture;
      set => Class14.resourceCulture = value;
    }

    internal static byte[] ap => (byte[]) Class14.ResourceManager.GetObject(nameof (ap), Class14.resourceCulture);

    internal static byte[] gsm_raptor => (byte[]) Class14.ResourceManager.GetObject(nameof (gsm_raptor), Class14.resourceCulture);

    internal static byte[] ios12_Reset_plist => (byte[]) Class14.ResourceManager.GetObject(nameof (ios12_Reset_plist), Class14.resourceCulture);

    internal static byte[] ios13_Reset_plist => (byte[]) Class14.ResourceManager.GetObject(nameof (ios13_Reset_plist), Class14.resourceCulture);

    internal static byte[] iuntethered => (byte[]) Class14.ResourceManager.GetObject(nameof (iuntethered), Class14.resourceCulture);

    internal static byte[] iuntethered1 => (byte[]) Class14.ResourceManager.GetObject(nameof (iuntethered1), Class14.resourceCulture);

    internal static byte[] ldrunps => (byte[]) Class14.ResourceManager.GetObject(nameof (ldrunps), Class14.resourceCulture);

    internal static byte[] lzma => (byte[]) Class14.ResourceManager.GetObject(nameof (lzma), Class14.resourceCulture);

    internal static byte[] plutil_iphone => (byte[]) Class14.ResourceManager.GetObject(nameof (plutil_iphone), Class14.resourceCulture);

    internal static byte[] purplebuddy => (byte[]) Class14.ResourceManager.GetObject(nameof (purplebuddy), Class14.resourceCulture);

    internal static byte[] restore => (byte[]) Class14.ResourceManager.GetObject(nameof (restore), Class14.resourceCulture);

    internal static byte[] substrate_gsm_1 => (byte[]) Class14.ResourceManager.GetObject(nameof (substrate_gsm_1), Class14.resourceCulture);

    internal static byte[] untethered => (byte[]) Class14.ResourceManager.GetObject(nameof (untethered), Class14.resourceCulture);

    internal static byte[] untethered1 => (byte[]) Class14.ResourceManager.GetObject(nameof (untethered1), Class14.resourceCulture);
  }
}
